$(document).ready(function(){
    $('.products-list').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        slidesToShow: 1,
        // centerPadding:'15%',
        responsive: [
            {
                breakpoint: 2160,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1152,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
    }).style.justifyContent = "space-between";
});

$(document).ready(function(){
    $(".youtube-thumbnail").click(function(){
        $(this).css("visibility", "hidden");
        // $("iframe").css("visibility", "visible");
    });
});